const yaml = require('js-yaml');
const fs   = require('fs');
const moment = require('moment');
const faker = require('faker');

let transactions = 200;
let currentDate = moment().subtract(6, 'month');

let increment = (24 * 180) / transactions;

const times = x => f => {
    if (x > 0) {
        f();
        times (x - 1) (f)
    }
};

let transactionAmt = () => {
    let n = faker.random.number();
    return parseInt(("" + n).slice(0,3))/100;
};

let generateTransaction = () => {
    currentDate.add(increment, 'hours');
    return {
        amount: transactionAmt(),
        bookingDate: currentDate.toISOString(),
        cdtDbtInd: 'DBIT',
        counterparty: faker.finance.iban(),
        remittanceInf: faker.random.arrayElement(['Apple', 'Mojang', 'Roblox', 'Google', 'Amazon', 'King'])
    }
};

let generateUser = () => {
    let transactions = [];
    times(100) (() => { transactions.push(generateTransaction()) });

    return {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        type: 'private',
        customerNumber: 123456789012,
        cardNumber: 1234567890123456,
        password: 1234567890,
        pin: 12345678,
        accounts: [
            {
                iban: 'IE83ULSB98601000000000',
                countryCode: 'IE',
                name: 'CurrentAccount',
                currency: 'EUR',
                balance: faker.random.number(),
                transactions: transactions
            }
        ]
    }
};

try {

    console.log(yaml.safeDump({
        users: [
            generateUser()
        ]
    }));

} catch (e) {
    console.log(e);
}

// Get document, or throw exception on error

