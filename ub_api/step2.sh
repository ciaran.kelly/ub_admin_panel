#! /bin/sh

STEP1=$1

REDIRECTURI=https://62a5311e-97d5-4ed0-81d2-94046cdd09ac.example.org/redirect
CLIENTID=C7X2MY2NvqNGOwsVYxnZt8Fh_9DY6Obw-t5b58gLlqw=
SECRET=vZmBGEUepomidR_7SbmmPuwi6cx6V4JVLb9UaGxXfqE=


TOKEN=$(curl -k -X POST https://ob.ulster.useinfinite.io/token -H 'Content-Type: application/x-www-form-urlencoded' -d "client_id=${CLIENTID}&client_secret=${SECRET}&redirect_uri=${REDIRECTURI}&grant_type=authorization_code&code=${STEP1}")

ACCESSTOKEN=$(echo $TOKEN | jq -r '.access_token')
echo $ACCESSTOKEN

echo $TOKEN > ./step2.token.json
