#! /bin/sh

ACCESSTOKEN=$(cat step2.token.json | jq -r '.access_token')

curl -k -X GET \
  https://ob.ulster.useinfinite.io/open-banking/v2.0/accounts \
  -H "Authorization: Bearer ${ACCESSTOKEN}" \
  -H 'x-fapi-financial-id: TBD'
